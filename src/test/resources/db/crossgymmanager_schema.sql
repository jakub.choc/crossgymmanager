DROP DATABASE IF EXISTS crossgymmanager;
CREATE DATABASE crossgymmanager;
USE crossgymmanager;
SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE if not exists box (
    id bigint NOT NULL AUTO_INCREMENT,
    gym_name varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE if not exists lesson (
    id bigint NOT NULL AUTO_INCREMENT,
    active bit(1) NOT NULL,
    capacity int NOT NULL,
    created datetime(6) DEFAULT NULL,
    `end` datetime(6) DEFAULT NULL,
    lesson_name varchar(255) DEFAULT NULL,
    lesson_plan varchar(255) DEFAULT NULL,
    `start` datetime(6) DEFAULT NULL,
    coach_id bigint DEFAULT NULL,
    lesson_box_id bigint DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FKk1ooptflb6uf7upsndx8dv7jd` (`coach_id`),
    KEY `FK1lxifhgf6s95gumu4sewnxh98` (`lesson_box_id`),
    CONSTRAINT `FK1lxifhgf6s95gumu4sewnxh98` FOREIGN KEY (`lesson_box_id`) REFERENCES `box` (`id`),
    CONSTRAINT `FKk1ooptflb6uf7upsndx8dv7jd` FOREIGN KEY (`coach_id`) REFERENCES `person` (`id`)
);

CREATE TABLE if not exists users (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `full_name` varchar(255) NOT NULL,
    `is_coach` bit(1) NOT NULL,
    `password` varchar(255) NOT NULL,
    `username` varchar(255) NOT NULL,
    `home_box_id` bigint DEFAULT NULL,
    `membership_id` bigint DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `UK_hgs572g5d2b39g9u7ym7pr95f` (`membership_id`),
    KEY `FKkhc0e7oagcjls51yaa0jyiab3` (`home_box_id`),
    CONSTRAINT `FKkhc0e7oagcjls51yaa0jyiab3`
    FOREIGN KEY (`home_box_id`)
    REFERENCES `box` (`id`),
    CONSTRAINT `FKrkd4v2r22h76788lpwopdcjo4`
    FOREIGN KEY (`membership_id`) REFERENCES `contract` (`id`)
);

CREATE TABLE if not exists `roles` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `role_name` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE if not exists lesson_person (
    lesson_id bigint NOT NULL,
    person_id bigint NOT NULL,
    KEY `FK42a5xo1yp4y923uhj2llamujs` (`person_id`),
    KEY `FKbtl9r2h3f7ise13am3kn72qqg` (`lesson_id`),
    CONSTRAINT `FK42a5xo1yp4y923uhj2llamujs`
    FOREIGN KEY (`person_id`) REFERENCES `users` (`id`),
    CONSTRAINT `FKbtl9r2h3f7ise13am3kn72qqg`
    FOREIGN KEY (`lesson_id`) REFERENCES `lesson` (`id`)
);

CREATE TABLE if not exists user_roles (
    user_id bigint NOT NULL,
    roles_id bigint NOT NULL,
    PRIMARY KEY (`user_id`,`roles_id`),
    KEY `FKrhfovtciq1l558cw6udg0h0d3` (`roles_id`),
    CONSTRAINT `FKhfh9dx7w3ubf1co1vdev94g3f`
    FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
    CONSTRAINT `FKrhfovtciq1l558cw6udg0h0d3` FOREIGN KEY (`roles_id`)
    REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE if not exists membership (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `contract_type` varchar(255) NOT NULL,
    `entry_amount` int DEFAULT NULL,
    `expiration_end` datetime(6) NOT NULL,
    `expiration_start` datetime(6) NOT NULL,
    `is_entry_option` bit(1) NOT NULL,
    `remaining_entry` int DEFAULT NULL,
    `user_id` bigint DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `UK_b0nlsgmn8885jfsyavlkau3cl` (`user_id`),
    CONSTRAINT `FKi6rphdb5rpnqnrp5twyk83jao`
    FOREIGN KEY (`user_id`)
    REFERENCES `users` (`id`)
);

INSERT INTO box(id, gym_name)
VALUES(1, 'bubalus')
;

INSERT INTO roles(id, role_name)
VALUES(1, 'ADMIN')
;

INSERT INTO user_roles(user_id, roles_id)
VALUES(1, 1)
;

INSERT INTO lesson(id, active, capacity, created, start, end, lesson_name, lesson_plan, coach_id, lesson_box_id)
VALUES(1, true, 12, '2021-01-01 10:00:00.000000', '2021-01-01 10:00:00.000000', '2021-01-01 11:00:00.000000', 'lesson', null, 1, 1)
;

INSERT INTO users(id, full_name, home_box_id, username, password, is_coach, membership_id)
VALUES(1, 'Tereza Sebkova', 1, 'terezasebkova@gmail.com', '$2a$10$a4eMcgcc.YAP/gwUwFWNzegqar36FnoRTWXnCconDvvC8G96BGUw.', true, null),
      (2, 'Jakub Choc', 1, 'jakubchoc@gmail.com', '$2a$10$a4eMcgcc.YAP/gwUwFWNzegqar36FnoRTWXnCconDvvC8G96BGUw.', false, 1)
;

INSERT INTO lesson_person(person_id, lesson_id)
VALUES(1, 1)
;

INSERT INTO membership(id, contract_type, entry_amount, expiration_end, expiration_start, is_entry_option, remaining_entry, user_id)
VALUES(1, "Annual Membership", null, '2022-01-01 10:00:00.000000', '2023-01-01 10:00:00.000000', false, null, 2)
;



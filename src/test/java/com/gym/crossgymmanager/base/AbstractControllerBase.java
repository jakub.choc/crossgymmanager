package com.gym.crossgymmanager.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import javax.sql.DataSource;


public class AbstractControllerBase extends AbstractTestContainersBase {

    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    private DataSource dataSource;

    protected String authenticateUserAndReturnToken() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        String body = "{\"username\":\"admin@gmail.com\",\"password\":\"1234\"}";

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                        .post("/user/authenticate")
                        .queryParams(params)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(body))
                        .andReturn();
        return result.getResponse().getHeader("Authorization");
    }

    protected ResultActions callPostRequest(String url, MultiValueMap<String, String> params, Object body) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders
                .post(url)
                .params(params)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(body)));
    }

    protected ResultActions callDeleteRequest(String url, MultiValueMap<String, String> params) throws Exception {
        String token = authenticateUserAndReturnToken();
        return mockMvc.perform(MockMvcRequestBuilders
                .delete(url)
                .header("Authorization", token)
                .params(params)
                .contentType(MediaType.APPLICATION_JSON));
    }

    protected ResultActions callGetRequest(String url, MultiValueMap<String, String> params, Object body) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .params(params)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(body)));
    }

    protected void callPostRequestAndExpect200(String url, Object body) throws Exception {
        callPostRequestAndExpect200(url, new LinkedMultiValueMap<>(), body);
    }

    protected void callPostRequestAndExpect200(String url, MultiValueMap<String, String> params, Object body) throws Exception {
        var result = callPostRequest(url, params, body).andReturn();
        Assertions.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
    }

    protected void callDeleteRequestAndExpect204(String url, MultiValueMap<String, String> params) throws Exception {
        var result = callDeleteRequest(url, params).andReturn();
        Assertions.assertEquals(HttpStatus.NO_CONTENT.value(), result.getResponse().getStatus());
    }

    protected void callPostRequestAndExpect201(String url, MultiValueMap<String, String> params, Object body) throws Exception {
        var result = callPostRequest(url, params, body).andReturn();
        Assertions.assertEquals(HttpStatus.CREATED.value(), result.getResponse().getStatus());
    }

    public <T> T callGetRequestWithResponse(String url, MultiValueMap<String, String> params, Class<T> clazz, Object body) throws Exception {
        String token = authenticateUserAndReturnToken();
        var result = mockMvc.perform(MockMvcRequestBuilders
                        .get(url)
                        .header("Authorization", token)
                        .queryParams(params)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(body))
                )
                .andReturn()
                .getResponse();
        Assertions.assertEquals(HttpStatus.OK.value(), result.getStatus());
        return mapToObject(result, clazz);
    }

    public <T> T callPostRequestWithResponseAndExpect201(String url, MultiValueMap<String, String> params, Class<T> clazz, Object body) throws Exception {
        String token = authenticateUserAndReturnToken();
        var result = mockMvc.perform(MockMvcRequestBuilders
                        .post(url)
                        .header("Authorization", token)
                        .queryParams(params)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(body))
                )
                .andReturn()
                .getResponse();
        Assertions.assertEquals(HttpStatus.CREATED.value(), result.getStatus());
        return mapToObject(result, clazz);
    }

    public <T> T callPutRequestWithResponse(String url, MultiValueMap<String, String> params, Class<T> clazz, Object body) throws Exception {
        String token = authenticateUserAndReturnToken();
        var result = mockMvc.perform(MockMvcRequestBuilders
                        .put(url)
                        .header("Authorization", token)
                        .queryParams(params)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(body))
                )
                .andReturn()
                .getResponse();
        Assertions.assertEquals(HttpStatus.OK.value(), result.getStatus());
        return mapToObject(result, clazz);
    }

    private <T> T mapToObject(MockHttpServletResponse response, Class<T> typeRef) {
        try {
            return objectMapper.readValue(response.getContentAsString(), typeRef);
        } catch (Exception e) {
            Assertions.fail(e);
            return null;
        }
    }

    protected void executeSqlScript(String sqlScript) {
        try (var connection = dataSource.getConnection()) {
            ScriptUtils.executeSqlScript(connection, new ByteArrayResource(sqlScript.getBytes(StandardCharsets.UTF_8)));
        } catch (SQLException e) {
            Assertions.fail(e);
        }
    }

}

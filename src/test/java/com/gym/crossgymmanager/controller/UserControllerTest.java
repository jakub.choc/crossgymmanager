package com.gym.crossgymmanager.controller;

import com.gym.crossgymmanager.base.AbstractControllerBase;
import com.gym.crossgymmanager.dto.UserDTO;
import com.gym.crossgymmanager.enums.MembershipType;
import lombok.AllArgsConstructor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@AllArgsConstructor
class UserControllerTest extends AbstractControllerBase {

    @Test
    void shouldReturnOkWitchResponse_WhenCallGetRequestOnUser() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        var response = callGetRequestWithResponse(
                "/user/1",
                params,
                UserDTO.class
        );
        var expected = UserDTO.builder()
                .id(1L)
                .username("terezasebkova@gmail.com")
                .fullName("Tereza Sebkova")
                .password(null)
                .isCoach(false)
                .membershipType(null)
                .build();
        Assertions.assertEquals(expected, response);
    }

    @Test
    void shouldReturnOk_WhenCallPostRequestRegisterUser() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        UserDTO user = UserDTO.builder()
                .username("karel.novak@gmail.com")
                .password("1234")
                .fullName("Karel Novak")
                .isCoach(false)
                .membershipType(MembershipType.QUARTER)
                .build();
        callPostRequestAndExpect201(
                "/user/register",
                params,
                user
        );
        deleteData();
    }

    @Test
    void shouldReturnOkWithResponse_WhenCallPutRequestEditUser() throws Exception {
        addUser();
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        UserDTO body = UserDTO.builder()
                .username("karel.novak@gmail.com")
                .password(null)
                .fullName("Karel Novotny")
                .isCoach(false)
                .build();
        var response = callPutRequestWithResponse(
                "/user/10",
                params,
                UserDTO.class,
                body
        );
        var expected = UserDTO.builder()
                .id(10L)
                .username("karel.novak@gmail.com")
                .fullName("Karel Novotny")
                .password(null)
                .isCoach(false)
                .build();
        Assertions.assertEquals(response, expected);
        deleteData();
    }

    @Test
    void shouldReturnNoContent_WhenCallDeleteRequestOnUser() throws Exception {
        addUser();
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        callDeleteRequestAndExpect204(
                "/user/10",
                params
        );
        deleteData();
    }



    private void deleteData() {
        executeSqlScript(
                """
                DELETE FROM users WHERE username = 'karel.novak@gmail.com'; 
                DELETE FROM membership WHERE id = 2;   
                """
        );
    }

    private void addUser() {
        executeSqlScript(
                """
                INSERT INTO users (id, full_name, home_box_id, username, password, is_coach)
                VALUES(10, 'Karel Novak', 1, 'karel.novak@gmail.com', '$2a$10$a4eMcgcc.YAP/gwUwFWNzegqar36FnoRTWXnCconDvvC8G96BGUw.', false);
                """
        );
    }

}

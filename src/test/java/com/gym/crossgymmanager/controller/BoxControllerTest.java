package com.gym.crossgymmanager.controller;

import com.gym.crossgymmanager.base.AbstractControllerBase;
import com.gym.crossgymmanager.dto.*;
import lombok.AllArgsConstructor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
class BoxControllerTest extends AbstractControllerBase {

    @Test
    void shouldReturnOkWitchResponse_WhenCallGetRequestOnBox() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        var response = callGetRequestWithResponse(
                "/box/1",
                params,
                BoxDTO.class,
                null
        );
        var expected = BoxDTO.builder().id(1L).gymName("bubalus").build();
        Assertions.assertEquals(response, expected);
    }

    @Test
    void shouldReturnOkWithBoxResponse_WhenCallPutRequestEditBox() throws Exception {
        addBox();
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        BoxDTO requestBody = BoxDTO.builder().gymName("POD").build();
        var response = callPutRequestWithResponse(
                "/box/2",
                params,
                BoxDTO.class,
                requestBody
        );
        var expected = BoxDTO.builder().id(2L).gymName("POD").build();
        Assertions.assertEquals(expected, response);
        deleteData();
    }

    @Test
    void shouldReturnOkWithListOfAllBoxes_WhenCallGetRequestAllBoxes() throws Exception {
        addBox();
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("gymName", "POD");
        var response = callGetRequestWithResponse(
                "/box/all",
                params,
                BoxesDTO.class,
                null
        );
        var boxes = List.of(
                BoxDTO.builder().id(2L).gymName("lospodolos").build(),
                BoxDTO.builder().id(3L).gymName("noname").build()
        );
        var expected = BoxesDTO.builder().boxes(boxes).build();
        Assertions.assertEquals(expected, response);
        deleteData();
    }

    @Test
    void shouldReturnOkWithListOfAllMemberBySpecificBox_WhenCallGetRequestAllMembers() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        var response = callGetRequestWithResponse(
                "/box/lessons",
                params,
                LessonsDTO.class,
                null
        );
        var expectedLessons = List.of(
        LessonDTO.builder()
                .id(1L)
                .lessonName("lesson")
                .created(LocalDateTime.of(2021,01, 01, 10, 00))
                .startDateTime(LocalDateTime.of(2021,01, 01, 10, 00))
                .endDateTime(LocalDateTime.of(2021,01, 01, 11, 00))
                .box("bubalus")
                .coach("terezasebkova@gmail.com")
                .loggedIn(List.of(
                        "terezasebkova@gmail.com"
                ))
                .capacity(12)
                .active(false)
                .lessonPlan(null)
                .build()
        );
        var expected = LessonsDTO.builder().lessons(expectedLessons).build();
        Assertions.assertEquals(expected, response);
        deleteData();
    }

    @Test
    void shouldReturnNoContent_WhenCallDeleteBox() throws Exception {
        addBox();
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("gymName", "POD");
        callDeleteRequestAndExpect204(
                "/box/3",
                params
        );
        deleteData();
    }

    @Test
    void shouldReturnOkWithNewBox_WhenCallPostRequestAddBox() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        BoxDTO requestBody = BoxDTO.builder().gymName("POD").build();
        var response = callPostRequestWithResponseAndExpect201(
                "/box/add",
                params,
                BoxDTO.class,
                requestBody
        );
        var expected = BoxDTO.builder().id(2L).gymName("POD").build();
        Assertions.assertEquals(expected, response);
        deleteData();
    }

    private void deleteData() {
        executeSqlScript(
                """
                DELETE FROM box WHERE id IN (2,3);
                """
        );
    }

    private void addBox() {
        executeSqlScript(
                """
                INSERT INTO box(id, gym_name)
                VALUES(2, 'lospodolos'), (3, 'noname');
                """
        );
    }
}

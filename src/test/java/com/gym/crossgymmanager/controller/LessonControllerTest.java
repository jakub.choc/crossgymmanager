package com.gym.crossgymmanager.controller;

import lombok.AllArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
import com.gym.crossgymmanager.base.AbstractControllerBase;
import com.gym.crossgymmanager.dto.LessonDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@AllArgsConstructor
class LessonControllerTest extends AbstractControllerBase {

    @Test
    void shouldReturnOkWitchResponse_WhenCallGetRequestOnLesson() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        var response = callGetRequestWithResponse(
                "/lesson/1",
                params,
                LessonDTO.class
        );
        var expected = LessonDTO.builder().id(1L).lessonName("lesson")
                .created(LocalDateTime.of(2021, 1,1,10,00))
                .startDateTime(LocalDateTime.of(2021,1,1,10,00))
                .endDateTime(LocalDateTime.of(2021,1,1,11,00))
                .box("bubalus")
                .coach("terezasebkova@gmail.com")
                .loggedIn(List.of("terezasebkova@gmail.com"))
                .capacity(12)
                .active(false)
                .lessonPlan(null)
                .build();
        Assertions.assertEquals(expected, response);
    }

    @Test
    void shouldReturnOkWitchResponseOfLesson_WhenCallPutRequestSignUpUserOnLesson() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("lessonId", "1");
        params.add("personId", "2");
        var response = callPutRequestWithResponse(
                "/lesson/sign-up",
                params,
                LessonDTO.class,
                null
        );
        var expected = LessonDTO.builder().id(1L).lessonName("lesson")
                .created(LocalDateTime.of(2021, 1,1,10,00))
                .startDateTime(LocalDateTime.of(2021,1,1,10,00))
                .endDateTime(LocalDateTime.of(2021,1,1,11,00))
                .box("bubalus")
                .coach("terezasebkova@gmail.com")
                .loggedIn(List.of("terezasebkova@gmail.com", "jakubchoc@gmail.com"))
                .capacity(12)
                .active(false)
                .lessonPlan(null)
                .build();
        Assertions.assertEquals(expected, response);
        cleanUpData();
    }

    @Test
    void shouldReturnOkWitchResponseOfLesson_WhenCallPutRequestSignOutUserFromLesson() throws Exception {
        setUpData();
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("lessonId", "1");
        params.add("personId", "2");
        var response = callPutRequestWithResponse(
                "/lesson/sign-out",
                params,
                LessonDTO.class,
                null
        );
        var expected = LessonDTO.builder().id(1L).lessonName("lesson")
                .created(LocalDateTime.of(2021, 1,1,10,00))
                .startDateTime(LocalDateTime.of(2021,1,1,10,00))
                .endDateTime(LocalDateTime.of(2021,1,1,11,00))
                .box("bubalus")
                .coach("terezasebkova@gmail.com")
                .loggedIn(List.of("terezasebkova@gmail.com"))
                .capacity(12)
                .active(false)
                .lessonPlan(null)
                .build();
        Assertions.assertEquals(expected, response);
    }

    @Test
    void shouldReturnOkWitchResponseOfLesson_WhenCallPostRequestCreateLesson() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        var requestBody = LessonDTO.builder().lessonName("afternoon lesson").lessonPlan(null).coach("Tereza Sebkova")
                .capacity(12).box("bubalus")
                .active(true).startDateTime(LocalDateTime.of(2023, 10,1, 14,00))
                .endDateTime(LocalDateTime.of(2023, 10,1, 15,00)).build();
        var response = callPostRequestWithResponseAndExpect201(
                "/lesson/add",
                params,
                LessonDTO.class,
                requestBody
        );
        var expected = LessonDTO.builder().id(2L).lessonName("afternoon lesson")
                .created(response.getCreated())
                .startDateTime(LocalDateTime.of(2023, 10,1, 14,00))
                .endDateTime(LocalDateTime.of(2023, 10,1, 15,00))
                .box("bubalus")
                .coach("terezasebkova@gmail.com")
                .loggedIn(List.of())
                .capacity(12)
                .active(true)
                .lessonPlan(null)
                .build();
        Assertions.assertEquals(expected, response);
        cleanUpData();
    }

    private void setUpData() {
        executeSqlScript(
                """
                INSERT INTO lesson_person(person_id, lesson_id)
                VALUES(2, 1);
                """
        );
    }

    private void cleanUpData() {
        executeSqlScript(
                """
                DELETE FROM lesson_person WHERE person_id = 2;
                DELETE FROM lesson WHERE id = 2;
                """
        );
    }
}

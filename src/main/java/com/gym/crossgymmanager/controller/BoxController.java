package com.gym.crossgymmanager.controller;

import com.gym.crossgymmanager.dto.*;
import lombok.AllArgsConstructor;
import com.gym.crossgymmanager.service.BoxFinderService;
import com.gym.crossgymmanager.service.BoxService;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/box")
public class BoxController {

    private final BoxService gymService;
    private final BoxFinderService boxFinderService;

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @ResponseStatus(HttpStatus.OK)
    public BoxDTO getBox(@PathVariable Long id) {
        return boxFinderService.getBoxById(id);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public BoxDTO editBox(@PathVariable Long id, @RequestBody BoxDTO boxDTO) {
        return gymService.editBox(id, boxDTO);
    }

    @GetMapping("/lessons")
    @ResponseStatus(HttpStatus.OK)
    public LessonsDTO boxLessons() {
        return boxFinderService.getAllBoxLessons();
    }

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public BoxesDTO allBoxess() {
        return boxFinderService.getAllBoxesEntity();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteBox(@PathVariable Long id) {
        gymService.deleteBox(id);
    }

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public BoxDTO addBox(@RequestBody BoxDTO boxDTO) {
        return gymService.addBox(boxDTO);
    }
}

package com.gym.crossgymmanager.controller;

import lombok.AllArgsConstructor;
import com.gym.crossgymmanager.dto.MembershipDTO;
import com.gym.crossgymmanager.service.MembershipFinderService;
import com.gym.crossgymmanager.service.MembershipService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/membership")
public class MembershipController {

    private final MembershipFinderService membershipFinderService;
    private final MembershipService membershipService;

    @GetMapping(path = "/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public MembershipDTO getMembership(@PathVariable Long userId) {
        return membershipFinderService.getMembershipByUserId(userId);
    }

    @PutMapping(path = "/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public MembershipDTO editMembership(@PathVariable Long userId, @RequestBody MembershipDTO membershipDTO) {
        return membershipService.editMembership(userId, membershipDTO);
    }
}

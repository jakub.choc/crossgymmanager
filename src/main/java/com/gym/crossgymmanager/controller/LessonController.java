package com.gym.crossgymmanager.controller;

import com.gym.crossgymmanager.enums.TimeRange;
import lombok.AllArgsConstructor;
import java.util.List;
import com.gym.crossgymmanager.dto.LessonDTO;
import com.gym.crossgymmanager.service.LessonFinderService;
import com.gym.crossgymmanager.service.LessonService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/lesson")
public class LessonController {

    private final LessonService lessonService;
    private final LessonFinderService lessonFinderService;

    @GetMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public LessonDTO getLesson(@PathVariable Long id) {
        return lessonFinderService.getLessonById(id);
    }

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<LessonDTO> getLessons(
            @RequestParam("time-range") TimeRange timeRange, @RequestParam boolean active) {
        return lessonFinderService.getLessons(timeRange, active);
    }

    @PutMapping("/sign-up/{lessonId}/{personId}")
    @ResponseStatus(HttpStatus.CREATED)
    public LessonDTO signUp(@PathVariable Long lessonId, @PathVariable Long personId) {
        return lessonService.signUpUserOnLesson(lessonId, personId);
    }

    @PutMapping("/sign-out/{lessonId}/{personId}")
    @ResponseStatus(HttpStatus.OK)
    public LessonDTO signOut(@PathVariable Long lessonId, @PathVariable Long personId) {
        return lessonService.signOutUserFromLesson(lessonId, personId);
    }

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public LessonDTO createLesson(@RequestBody LessonDTO lessonDTO) {
        return lessonService.createLesson(lessonDTO);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public LessonDTO editLesson(@PathVariable Long id, @RequestBody LessonDTO lessonDTO) {
        return lessonService.editLesson(id, lessonDTO);
    }

    @DeleteMapping ("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteLesson(@PathVariable Long id) {
        lessonService.deleteLesson(id);
    }

}

package com.gym.crossgymmanager.controller;

import com.gym.crossgymmanager.dto.UserDTO;
import com.gym.crossgymmanager.service.UserFinderService;
import com.gym.crossgymmanager.service.UserService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/user")
public class UserController {

    private final UserService userService;
    private final UserFinderService userFinderService;

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDTO getPerson(@PathVariable Long id) {
        return userFinderService.getUser(id);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDTO editPerson(@PathVariable Long id, @RequestBody UserDTO personDTO) {
        return userService.editUser(id, personDTO);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePerson(@PathVariable Long id) {
        userService.deleteUser(id);
    }

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public void createUser(@Valid @RequestBody UserDTO userDTO) {
        userService.addUser(userDTO);
    }
}

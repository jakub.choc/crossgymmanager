package com.gym.crossgymmanager.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import java.time.ZonedDateTime;

@Entity(name = "membership")
@Getter
@Setter
public class Membership {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String contractType;

    @NotNull
    private ZonedDateTime expirationStart;

    @NotNull
    private ZonedDateTime expirationEnd;

    @NotNull
    private boolean isEntryOption;

    @Column
    private int entryAmount;

    @Column
    private int remainingEntry;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

}

package com.gym.crossgymmanager.entity.repository;

import com.gym.crossgymmanager.entity.Box;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoxRepository extends JpaRepository<Box, Long> {
}

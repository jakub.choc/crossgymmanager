package com.gym.crossgymmanager.entity.repository;

import java.util.Optional;
import com.gym.crossgymmanager.entity.Membership;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MembershipRepository extends JpaRepository<Membership, Long> {

    Optional<Membership> findMembershipByUserId(Long UserId);
}

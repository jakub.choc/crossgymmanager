package com.gym.crossgymmanager.entity.repository;

import com.gym.crossgymmanager.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query(
            value = """
            SELECT * FROM users
            WHERE home_box_id =:boxId 
            AND full_name =:fullName 
            AND is_coach = true
            """,
            nativeQuery = true
    )
    Optional<User> findCoachForSpecificGym(
            @Param("boxId") Long boxId,
            @Param("fullName") String fullName
    );

    @Query(
            value = """
            SELECT * FROM users
            WHERE home_box_id =:boxId 
            AND username =:username 
            AND is_coach = 'false'
            """,
            nativeQuery = true
    )
    Optional<User> findUserForSpecificGym(
            @Param("boxId") Long boxId,
            @Param("username") String username
    );

    Optional<User> findUserByUsername(String username);

}

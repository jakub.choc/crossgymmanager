package com.gym.crossgymmanager.entity.repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import com.gym.crossgymmanager.entity.Lesson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface LessonRepository extends JpaRepository<Lesson, Long> {

    @Query(
            value = """
            SELECT * FROM lesson
            WHERE lesson_box_id = :boxId
            AND start(`timestamp`) = CURDATE()
            AND active = :active
            """,
            nativeQuery = true
    )
    Optional<List<Lesson>> findAllLessonsForCurrentDay(
            @Param("boxId") Long boxId,
            @Param("active") boolean active
    );

    @Query(
            value = """
            SELECT * FROM lesson
            WHERE lesson_box_id = :boxId
            AND active = :active
            """,
            nativeQuery = true
    )
    Optional<List<Lesson>> findAllByBoxId(
            @Param("boxId") Long boxId,
            @Param("active") boolean active
    );

    @Query(
            value = """
            SELECT * FROM lesson
            WHERE YEARWEEK(:date)=YEARWEEK(NOW())
            AND active = :active
            AND box_id = :boxId
            """,
            nativeQuery = true
    )
    Optional<List<Lesson>> findAllLessonsForCurrentWeek(
            @Param("boxId") Long boxId,
            @Param("active") boolean active
    );

    @Query(
            value = """
            SELECT * FROM lesson
            WHERE start_date >= datefromparts(year(getdate()), month(getdate()), 1)
            AND active = :active
            AND lesson_box_id = :boxId
            """,
            nativeQuery = true
    )
    Optional<List<Lesson>> findAllLessonsForCurrentMonth(
            @Param("boxId") Long boxId,
            @Param("active") boolean active
    );

    @Query(
            value = """
            SELECT * FROM lesson
            WHERE start_date >= datefromparts(year(getdate()))
            AND active = :active
            AND lesson_box_id = :boxId
            """,
            nativeQuery = true
    )
    Optional<List<Lesson>> findAllLessonsForCurrentYear(
            @Param("boxId") Long boxId,
            @Param("active") boolean active
    );

    @Query(
            value = """
            SELECT * FROM lesson
            WHERE end <= :datetime
            AND active = :active
            """,
            nativeQuery = true
    )
    Optional<List<Lesson>> findAllLessonsForActiveCheck(
            @Param("active") boolean active,
            @Param("datetime")ZonedDateTime dateTime
    );
}

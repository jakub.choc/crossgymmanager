package com.gym.crossgymmanager.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity(name = "box")
@Getter
@Setter
public class Box {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String gymName;

    @OneToMany(mappedBy = "lessonBox")
    private List<Lesson> lessons;

    @OneToMany(mappedBy = "homeBox")
    private List<User> members;

}

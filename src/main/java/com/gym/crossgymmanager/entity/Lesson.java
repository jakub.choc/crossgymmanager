package com.gym.crossgymmanager.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "lesson")
@Getter
@Setter
public class Lesson {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String lessonName;

    private ZonedDateTime created = ZonedDateTime.now();

    private ZonedDateTime start;

    private ZonedDateTime end;

    @ManyToOne
    private Box lessonBox;

    @ManyToOne
    private User coach;

    @ManyToMany
    @JoinTable(name = "lesson_person",
            joinColumns = @JoinColumn(name = "lesson_id"),
            inverseJoinColumns = @JoinColumn(name = "person_id")
    )
    private List<User> loggedIn = new ArrayList<>();

    private int capacity;

    private boolean active = true;

    private String lessonPlan;

}

package com.gym.crossgymmanager.util;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import com.gym.crossgymmanager.service.LessonService;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Slf4j
@AllArgsConstructor
@Component
public class DatabaseCronJob {

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    private final LessonService lessonService;

    @Bean
    public void startCronJob() {
        scheduler.scheduleAtFixedRate(this::checkDatabase, 0, 1, TimeUnit.HOURS);
    }

    private void checkDatabase() {
        log.info("Cron jobs running...");
        lessonService.checkLessonStatus();
    }

}

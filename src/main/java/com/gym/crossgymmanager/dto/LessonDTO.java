package com.gym.crossgymmanager.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LessonDTO {

    private Long id;
    private String lessonName;
    private LocalDateTime created;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    private String box;
    private String coach;
    private List<String> loggedIn;
    private int capacity;
    private boolean active;
    private String lessonPlan;
}

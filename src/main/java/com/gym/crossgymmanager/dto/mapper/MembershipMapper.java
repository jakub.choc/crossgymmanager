package com.gym.crossgymmanager.dto.mapper;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import com.gym.crossgymmanager.dto.MembershipDTO;
import com.gym.crossgymmanager.entity.Membership;
import com.gym.crossgymmanager.util.CrossGymManagerConstants;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface MembershipMapper {

    @Mapping(target = "expirationStart", expression = "java(parseZonedDateTimeIntoLocalDateTime(source.getExpirationStart()))")
    @Mapping(target = "expirationEnd", expression = "java(parseZonedDateTimeIntoLocalDateTime(source.getExpirationEnd()))")
    @Mapping(target = "isEntryOption", source = "entryOption")
    MembershipDTO toDTO(Membership source);

    @Mapping(target = "expirationStart", expression = "java(parseLocalDateTimeIntoZonedDateTime(source.getExpirationStart()))")
    @Mapping(target = "expirationEnd", expression = "java(parseLocalDateTimeIntoZonedDateTime(source.getExpirationEnd()))")
    @Mapping(target = "user", ignore = true)
    Membership toEntity(MembershipDTO source);

    @Mapping(target = "user", ignore = true)
    void updateEntity(MembershipDTO source, @MappingTarget Membership target);

    default ZonedDateTime parseLocalDateTimeIntoZonedDateTime(LocalDateTime localDateTime) {
        return ZonedDateTime.of(localDateTime, ZoneId.of(CrossGymManagerConstants.PRAGUE_TIME_ZONE));
    }

    default LocalDateTime parseZonedDateTimeIntoLocalDateTime(ZonedDateTime zonedDateTime) {
        return zonedDateTime.toLocalDateTime();
    }
}

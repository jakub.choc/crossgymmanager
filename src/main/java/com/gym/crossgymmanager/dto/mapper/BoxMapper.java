package com.gym.crossgymmanager.dto.mapper;

import com.gym.crossgymmanager.dto.BoxDTO;
import com.gym.crossgymmanager.entity.Box;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface BoxMapper {

    BoxDTO toDTO(Box dto);

    Box toEntity(BoxDTO boxDTO);

    void updateEntity(BoxDTO source, @MappingTarget Box target);

}

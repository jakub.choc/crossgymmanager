package com.gym.crossgymmanager.dto.mapper;

import com.gym.crossgymmanager.dto.UserDTO;
import com.gym.crossgymmanager.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(target = "password", ignore = true)
    User toEntity(UserDTO source);

    @Mapping(target = "password", ignore = true)
    UserDTO toDTO(User person);

    void updateEntity(UserDTO source, @MappingTarget User target);
}

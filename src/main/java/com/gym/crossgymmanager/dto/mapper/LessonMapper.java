package com.gym.crossgymmanager.dto.mapper;

import com.gym.crossgymmanager.dto.LessonDTO;
import com.gym.crossgymmanager.entity.Lesson;
import com.gym.crossgymmanager.entity.User;
import com.gym.crossgymmanager.util.CrossGymManagerConstants;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Mapper(componentModel = "spring")
public interface LessonMapper {

    @Mapping(target = "coach", source = "coach.username")
    @Mapping(target = "loggedIn", expression = "java(getRegisteredId(source))")
    @Mapping(target = "box", source = "lessonBox.gymName")
    @Mapping(target = "startDateTime", expression = "java(parseZonedDateTimeIntoLocalDateTime(source.getStart()))")
    @Mapping(target = "endDateTime", expression = "java(parseZonedDateTimeIntoLocalDateTime(source.getEnd()))")
    LessonDTO toDTO(Lesson source);

    @Mapping(target = "lessonBox", ignore = true)
    @Mapping(target = "loggedIn", ignore = true)
    @Mapping(target = "coach", ignore = true)
    @Mapping(target = "start", expression = "java(parseLocalDateTimeIntoZonedDateTime(source.getStartDateTime()))")
    @Mapping(target = "end", expression = "java(parseLocalDateTimeIntoZonedDateTime(source.getEndDateTime()))")
    @Mapping(target = "created", ignore = true)
    Lesson toEntity(LessonDTO source);

    default List<String> getRegisteredId(Lesson source) {
        if (source.getLoggedIn() == null) {
            Collections.emptyList();
        }
        List<String> result = new ArrayList<>();
        for (User person : source.getLoggedIn()) {
            result.add(person.getUsername());
        }
        return result;
    }

    default ZonedDateTime parseLocalDateTimeIntoZonedDateTime(LocalDateTime localDateTime) {
        return ZonedDateTime.of(localDateTime, ZoneId.of(CrossGymManagerConstants.PRAGUE_TIME_ZONE));
    }

    default LocalDateTime parseZonedDateTimeIntoLocalDateTime(ZonedDateTime zonedDateTime) {
        return zonedDateTime.toLocalDateTime();
    }

}

package com.gym.crossgymmanager.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class ListDTO {
    private List<?> objects;
}

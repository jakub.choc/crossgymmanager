package com.gym.crossgymmanager.dto;

import lombok.Builder;
import lombok.Data;
import java.time.LocalDateTime;

@Builder
@Data
public class MembershipDTO {

    private Long id;
    private String contractType;
    private LocalDateTime expirationStart;
    private LocalDateTime expirationEnd;
    private boolean isEntryOption;
    private int entryAmount;
    private int remainingEntry;
}

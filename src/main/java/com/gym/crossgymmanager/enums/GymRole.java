package com.gym.crossgymmanager.enums;

public enum GymRole {

    CUSTOMER,
    COACH,
    GYM_ADMIN,

}

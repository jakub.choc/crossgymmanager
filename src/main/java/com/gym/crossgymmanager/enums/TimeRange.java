package com.gym.crossgymmanager.enums;

public enum TimeRange {
    ALL,
    DAY,
    WEEK,
    MONTH,
    YEAR
}

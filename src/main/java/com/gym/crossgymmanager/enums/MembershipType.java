package com.gym.crossgymmanager.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum MembershipType {

    TEN("Ten Entrance", 6 , true, 10, false),
    YEAR("Annual Membership", 12 , false, 0, true),
    QUARTER("Quarter year Membership", 3, false, 0, true),
    HALF("Half year Membership", 6, false, 0, true);

    public String contractName;

    public int monthDuration;

    public boolean entryOption;

    public int entryAmount;

    public boolean hasMonthlyPayment;
}

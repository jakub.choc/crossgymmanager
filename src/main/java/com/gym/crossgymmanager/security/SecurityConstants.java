package com.gym.crossgymmanager.security;

public class SecurityConstants {
    public static final String REGISTER_PATH = "/user/register";
    public static final String LOGIN_PATH = "/authenticate";
    public static final int TOKEN_EXPIRATION = 7200000;
    public static final String SECRET_KEY = "bQeThWmZq4t7w!z$C&F)J@NcRfUjXn2r5u8x/A?D*G-KaPdSgVkYp3s6v9y$B&E)";
    public static final String AUTHORIZATION = "Authorization";
    public static final String BEARER = "Bearer ";
    //Swagger
    public static final String SWAGGER = "/swagger-ui/**";
    public static final String API_DOCS = "/api-docs/**";

    private SecurityConstants() {

    }

}

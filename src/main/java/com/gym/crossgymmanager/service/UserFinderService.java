package com.gym.crossgymmanager.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import java.util.Optional;
import com.gym.crossgymmanager.dto.UserDTO;
import com.gym.crossgymmanager.dto.mapper.UserMapper;
import com.gym.crossgymmanager.entity.User;
import com.gym.crossgymmanager.entity.repository.UserRepository;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class UserFinderService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserDTO getUser(Long userId) {
        Optional<User> user = userRepository.findById(userId);
        User unwrapUser = unwrapUser(user, userId);
        return userMapper.toDTO(unwrapUser);
    }

    public User getUser(String username) {
        Optional<User> user = userRepository.findUserByUsername(username);
        return unwrapUser(user, 404L);
    }

    public User findCoachForSpecificBox(Long boxId, String fullName) {
        Optional<User> coach = userRepository.findCoachForSpecificGym(boxId, fullName);
        return unwrapUser(coach, 404L);

    }

    public User findUserEntity(Long userId) {
        Optional<User> user = userRepository.findById(userId);
        return unwrapUser(user, userId);
    }

    public boolean isAlreadyRegistered(String username) {
        //User authenticatedUser = getAuthenticatedUser();
        User user = userRepository.findUserForSpecificGym(1L, username).orElse(null);
        return user != null;
    }

    static User unwrapUser(Optional<User> entity, Long id) {
        if (entity.isPresent()) {
            return entity.get();
        }
        throw new com.gym.crossgymmanager.exception.EntityNotFoundException(id, User.class);
    }

    public User getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();
            return getUser(currentUserName);
        }
        throw new RuntimeException("Could not find authenticated user");
    }

}

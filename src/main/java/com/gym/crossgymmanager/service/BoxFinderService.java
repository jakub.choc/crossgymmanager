package com.gym.crossgymmanager.service;

import com.gym.crossgymmanager.dto.*;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.Optional;

import com.gym.crossgymmanager.dto.mapper.BoxMapper;
import com.gym.crossgymmanager.dto.mapper.LessonMapper;
import com.gym.crossgymmanager.entity.Box;
import com.gym.crossgymmanager.entity.repository.BoxRepository;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class BoxFinderService {

    private final BoxRepository boxRepository;
    private final BoxMapper boxMapper;
    private final LessonMapper lessonMapper;

    public BoxDTO getBoxById(Long id) {
        Optional<Box> box = boxRepository.findById(id);
        Box unwrapBox = unwrapBox(box, id);
        return boxMapper.toDTO(unwrapBox);
    }

    public Box getBoxEntityById(Long id) {
        Optional<Box> box = boxRepository.findById(id);
        return unwrapBox(box, id);
    }

    public LessonsDTO getAllBoxLessons() {
        Box box = getBoxEntityById(1L);
        return LessonsDTO.builder()
                .lessons(box.getLessons().stream().map(lessonMapper::toDTO).toList())
                .build();
    }

    public List<Box> getAllBoxes() {
        List<Box> boxes = boxRepository.findAll();
        if (boxes == null) {
            throw new EntityNotFoundException();
        }
    }

    public List<BoxDTO> getAllBoxesDTO() {
        boxRepository.findAll().stream().map(BoxMapper::toDTO).toList();
    }

    static Box unwrapBox(Optional<Box> entity, Long id) {
        if (entity.isPresent()) return entity.get();
        else throw new com.gym.crossgymmanager.exception.EntityNotFoundException(id, Box.class);
    }
}

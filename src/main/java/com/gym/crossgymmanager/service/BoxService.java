package com.gym.crossgymmanager.service;

import lombok.AllArgsConstructor;
import com.gym.crossgymmanager.dto.BoxDTO;
import com.gym.crossgymmanager.dto.mapper.BoxMapper;
import com.gym.crossgymmanager.entity.Box;
import com.gym.crossgymmanager.entity.repository.BoxRepository;
import org.springframework.stereotype.Service;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class BoxService {

    private final BoxRepository boxRepository;
    private final BoxMapper boxMapper;
    private final BoxFinderService boxFinderService;

    private static final Logger log = Logger.getLogger(BoxService.class);

    public BoxDTO editBox(Long id, BoxDTO boxDTO) {
        Optional<Box> box = boxRepository.findById(id);
        Box unwrapBox = boxFinderService.unwrapBox(box, id);
        boxDTO.setId(id);
        boxMapper.updateEntity(boxDTO, unwrapBox);
        Box edited = boxRepository.save(unwrapBox);
        return boxMapper.toDTO(edited);
    }

    public void deleteBox(Long id) {
        boxRepository.deleteById(id);
    }

    public BoxDTO addBox(BoxDTO boxDTO) {
        List<Box> boxes = boxFinderService.getAllBoxesEntity();
        boolean unavailable = boxes.stream().map(Box::getGymName).toList().contains(boxDTO.getGymName());
        if (unavailable) {
            throw new IllegalArgumentException();
        }
        Box box = boxMapper.toEntity(boxDTO);
        Box newBox = boxRepository.save(box);
        return boxMapper.toDTO(newBox);
    }

}

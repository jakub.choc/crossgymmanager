package com.gym.crossgymmanager.service;

import com.gym.crossgymmanager.entity.Box;
import com.gym.crossgymmanager.entity.User;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import com.gym.crossgymmanager.dto.LessonDTO;
import com.gym.crossgymmanager.dto.mapper.LessonMapper;
import com.gym.crossgymmanager.entity.Lesson;
import com.gym.crossgymmanager.entity.repository.LessonRepository;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class LessonService {

    private final BoxFinderService boxFinderService;
    private final UserFinderService userFinderService;
    private final LessonFinderService lessonFinderService;
    private final MembershipFinderService membershipFinderService;
    private final LessonRepository lessonRepository;
    private final LessonMapper lessonMapper;

    public LessonDTO signUpUserOnLesson(Long lessonId, Long userId) {
        User user = userFinderService.findUserEntity(userId);
        Lesson lesson = lessonFinderService.getLessonEntityById(lessonId);
        List<User> loggedIn = lesson.getLoggedIn();
        loggedIn.add(user);
        lesson.setLoggedIn(loggedIn);
        Lesson entity = lessonRepository.save(lesson);
        return lessonMapper.toDTO(entity);
    }

    public LessonDTO signOutUserFromLesson(Long lessonId, Long userId) {
        User user = userFinderService.findUserEntity(userId);
        Lesson lesson = lessonFinderService.getLessonEntityById(lessonId);
        List<User> loggedIn = lesson.getLoggedIn();
        loggedIn.remove(user);
        lesson.setLoggedIn(loggedIn);
        Lesson entity = lessonRepository.save(lesson);
        return lessonMapper.toDTO(entity);
    }

    public LessonDTO createLesson(LessonDTO lessonDTO) {
        User authenticatedUser = userFinderService.getAuthenticatedUser();
        Lesson lesson = lessonMapper.toEntity(lessonDTO);
        Box box = boxFinderService.getBoxEntityById(authenticatedUser.getHomeBox().getId());
        User coach = userFinderService.findCoachForSpecificBox(box.getId(), lessonDTO.getCoach());
        lesson.setLessonBox(box);
        lesson.setCoach(coach);
        Lesson entity = lessonRepository.save(lesson);
        return lessonMapper.toDTO(entity);
    }

    public LessonDTO editLesson(Long lessonId, LessonDTO lessonDTO) {
        User authenticatedUser = userFinderService.getAuthenticatedUser();
        Lesson lesson = lessonFinderService.getLessonEntityById(lessonId);
        if (lesson == null) {
            throw new EntityNotFoundException();
        }
        lessonDTO.setId(lessonId);
        Lesson entity = lessonMapper.toEntity(lessonDTO);
        entity.setLessonBox(lesson.getLessonBox());
        entity.setLoggedIn(lesson.getLoggedIn());
        entity.setCoach(lesson.getCoach());
        if (!lessonDTO.getCoach().equals(lesson.getCoach().getUsername())) {
            User coach = userFinderService.findCoachForSpecificBox(
                    authenticatedUser.getHomeBox().getId(), lessonDTO.getCoach());
            entity.setCoach(coach);
        }
        Lesson editedLesson = lessonRepository.save(entity);
        return lessonMapper.toDTO(editedLesson);
    }

    public void deleteLesson(Long id) {
        lessonRepository.deleteById(id);
    }

    public void membershipProcess(Long userId) {
        boolean activeMembership = membershipFinderService.hasUserActiveMembership(userId);
        membershipFinderService.getMembershipByUserId(userId);
        if (activeMembership) {

        }

    }

    public void checkLessonStatus() {
        Optional<List<Lesson>> lessons = lessonRepository.findAllLessonsForActiveCheck(true, ZonedDateTime.now());
        if (lessons != null) {
            List<Lesson> activeLessons = lessons.get();
            activeLessons.forEach(l -> l.setActive(false));
            lessonRepository.saveAll(activeLessons);
            log.info(activeLessons.size() + "lessons was set as inactive");
        }
        log.info("No one lesson was found for status change");
    }

}

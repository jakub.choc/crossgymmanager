package com.gym.crossgymmanager.service;

import lombok.AllArgsConstructor;
import com.gym.crossgymmanager.dto.MembershipDTO;
import com.gym.crossgymmanager.dto.mapper.MembershipMapper;
import com.gym.crossgymmanager.entity.Membership;
import com.gym.crossgymmanager.entity.repository.MembershipRepository;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MembershipService {

    private final MembershipRepository membershipRepository;
    private final MembershipFinderService membershipFinderService;
    private final MembershipMapper membershipMapper;

    public Membership saveMembership(Membership membership) {
        return membershipRepository.save(membership);
    }

    public MembershipDTO editMembership(Long userId, MembershipDTO membershipDTO) {
        Membership membership = membershipFinderService.getMembershipEntityByUserId(userId);
        membershipDTO.setId(membershipDTO.getId());
        membershipMapper.updateEntity(membershipDTO, membership); //doresit usera
        Membership editedMembership = membershipRepository.save(membership);
        return membershipMapper.toDTO(editedMembership);
    }

    public void removeOneEntryFromMembership(Long userId) {
        Membership membership = membershipFinderService.getMembershipEntityByUserId(userId);
        membership.setRemainingEntry(membership.getRemainingEntry() - 1);
        saveMembership(membership);
    }

    public void addOneEntryFromMembership(Long userId) {
        Membership membership = membershipFinderService.getMembershipEntityByUserId(userId);
        membership.setRemainingEntry(membership.getRemainingEntry() + 1);
        saveMembership(membership);
    }

}

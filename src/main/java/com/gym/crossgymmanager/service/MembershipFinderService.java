package com.gym.crossgymmanager.service;

import lombok.AllArgsConstructor;
import java.time.ZonedDateTime;
import java.util.Optional;
import com.gym.crossgymmanager.dto.MembershipDTO;
import com.gym.crossgymmanager.dto.mapper.MembershipMapper;
import com.gym.crossgymmanager.entity.Membership;
import com.gym.crossgymmanager.entity.repository.MembershipRepository;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MembershipFinderService {

    private final MembershipRepository membershipRepository;
    private final MembershipMapper membershipMapper;

    public MembershipDTO getMembershipByUserId(Long userId) {
        Optional<Membership> membership = membershipRepository.findById(userId);
        Membership membershipEntity = unwrapMembership(membership, 404L);
        return membershipMapper.toDTO(membershipEntity);
    }

    public boolean hasUserActiveMembership(Long userId) {
        Membership membership = getMembershipEntityByUserId(userId);
        if (membership.isEntryOption()) {
            return membership.getEntryAmount() <= 0;
        } else {
            return membership.getExpirationEnd().isAfter(ZonedDateTime.now());
        }
    }

    public Membership getMembershipEntityByUserId(Long userId) {
        Optional<Membership> membership = membershipRepository.findById(userId);
        return unwrapMembership(membership, 404L);
    }

    static Membership unwrapMembership(Optional<Membership> entity, Long id) {
        if (entity.isPresent()) {
            return entity.get();
        }
        throw new com.gym.crossgymmanager.exception.EntityNotFoundException(id, Membership.class);
    }
}

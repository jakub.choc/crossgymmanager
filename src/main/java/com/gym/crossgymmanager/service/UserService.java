package com.gym.crossgymmanager.service;

import com.gym.crossgymmanager.dto.UserDTO;
import com.gym.crossgymmanager.dto.mapper.UserMapper;
import com.gym.crossgymmanager.entity.Box;
import com.gym.crossgymmanager.entity.Membership;
import com.gym.crossgymmanager.entity.User;
import com.gym.crossgymmanager.entity.repository.UserRepository;
import com.gym.crossgymmanager.enums.MembershipType;
import com.gym.crossgymmanager.exception.UserAlreadyExistException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.Optional;

@Slf4j
@Service
@AllArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final BoxFinderService boxFinderService;
    private final UserFinderService userFinderService;
    private final MembershipService membershipService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final UserMapper userMapper;

    public void addUser(UserDTO source) {
        if (!userFinderService.isAlreadyRegistered(source.getUsername())) {
            User user = userMapper.toEntity(source);
            Box box = boxFinderService.getBoxEntityById(1L);
            user.setHomeBox(box);
            user.setPassword(bCryptPasswordEncoder.encode(source.getPassword()));
            if (source.getMembershipType() != null) {
                Membership newMembership = membershipService.saveMembership(createMembership(source.getMembershipType()));
                user.setMembership(newMembership);
            }
            userRepository.save(user);
        } else {
            log.error("Registered user already exist");
            throw new UserAlreadyExistException(source.getUsername());
        }
    }

    public UserDTO editUser(Long userId, UserDTO personDTO) {
        Optional<User> user = userRepository.findById(userId);
        User unwrapUser = userFinderService.unwrapUser(user, userId);
        personDTO.setId(userId);
        personDTO.setPassword(unwrapUser.getPassword());
        userMapper.updateEntity(personDTO, unwrapUser);
        User editedPerson = userRepository.save(unwrapUser);
        return userMapper.toDTO(editedPerson);
    }

    public void deleteUser(Long userId) {
        userRepository.deleteById(userId);
    }

    private Membership createMembership(MembershipType membershipType) {
        Membership membership = new Membership();
        membership.setContractType(membershipType.name());
        membership.setExpirationStart(ZonedDateTime.now());
        membership.setExpirationEnd(ZonedDateTime.now().plusMonths(membershipType.monthDuration));
        if (membershipType.entryOption) {
            membership.setEntryAmount(membershipType.entryAmount);
            membership.setRemainingEntry(membershipType.entryAmount);
        }
        return membership;
    }

}

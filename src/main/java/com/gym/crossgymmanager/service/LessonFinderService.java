package com.gym.crossgymmanager.service;

import com.gym.crossgymmanager.dto.LessonsDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import com.gym.crossgymmanager.dto.LessonDTO;
import com.gym.crossgymmanager.dto.mapper.LessonMapper;
import com.gym.crossgymmanager.entity.Lesson;
import com.gym.crossgymmanager.entity.User;
import com.gym.crossgymmanager.entity.repository.LessonRepository;
import com.gym.crossgymmanager.enums.TimeRange;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class LessonFinderService {

    private final LessonRepository lessonRepository;
    private final LessonMapper lessonMapper;
    private final UserFinderService userFinderService;

    public LessonDTO getLessonById(Long id) {
        Optional<Lesson> lesson = lessonRepository.findById(id);
        Lesson unwrapLesson = unwrapLesson(lesson, id);
        return lessonMapper.toDTO(unwrapLesson);
    }

    public LessonsDTO getLessons(TimeRange timeRange, boolean active) {
        User authenticatedUser = userFinderService.getAuthenticatedUser();
        var lessons =  getLessonsByCriteria(timeRange, active, authenticatedUser.getHomeBox().getId())
                .stream()
                .map(lessonMapper::toDTO)
                .toList();
        return LessonsDTO.builder().lessons(lessons).build();
    }

    public Lesson getLessonEntityById(Long id) {
        Optional<Lesson> lesson = lessonRepository.findById(id);
        return unwrapLesson(lesson, id);
    }

    public List<Lesson> getLessonsByCriteria(TimeRange timeRange, boolean active, Long boxId) {
        Optional<List<Lesson>> lessons;
        switch (timeRange) {
            case ALL:
                lessons = lessonRepository.findAllByBoxId(boxId, active);
                break;
            case DAY:
                lessons = lessonRepository.findAllLessonsForCurrentDay(boxId, active);
                break;
            case WEEK:
                lessons = lessonRepository.findAllLessonsForCurrentWeek(boxId, active);
                break;
            case MONTH:
                lessons = lessonRepository.findAllLessonsForCurrentMonth(boxId, active);
                break;
            case YEAR:
                lessons = lessonRepository.findAllLessonsForCurrentYear(boxId, active);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + timeRange);
        }
        return lessons.orElse(Collections.emptyList());
    }

    static Lesson unwrapLesson(Optional<Lesson> entity, Long id) {
        if (entity.isPresent()) {
            return entity.get();
        } else {
            throw new com.gym.crossgymmanager.exception.EntityNotFoundException(id, Lesson.class);
        }
    }
}
